#!/usr/local/bin/python
"""
A pre-commit hook for git that uses Pylint for automated code review.

If any python file's rating falls below the ``PYLINT_PASS_THRESHOLD``, this
script will return nonzero and the commit will be rejected. 

Currently, the threshold is set to 0. So no commit will be rejected. 

This script must be located at ``$REPO/.git/hooks/pre-commit`` and be
executable.

The script is modified based off 
https://fitzgeraldnick.com/2009/12/03/git-hooks-and-pylint.html
"""

import os
import re
import sys

from subprocess import Popen, PIPE

# Threshold for code to pass the Pylint test. 10 is the highest score Pylint
# will give to any peice of code.
PYLINT_PASS_THRESHOLD = 0

def main():
    """Checks your git commit with Pylint!"""
    # Run the git command that gets the filenames of every file that has been
    # locally modified since the last commit.
    sub = Popen("git diff --staged --name-only HEAD".split(),
                stdout=PIPE)
    sub.wait()


    # Filter out non-python or deleted files.
    # Popen returns byte sequence and newline. b'xx.py'\n - reason for using decode and strip()
    py_files_changed = [file
                        for file in [f.decode('ascii').strip() for f in sub.stdout.readlines()]
                        if (file.endswith(".py") and os.path.exists(file))
                            or is_py_script(file)]
    print ("found changed .py: ", py_files_changed)
    print ("=============================================")

    # Run Pylint on each file, collect the results, and display them for the user.
    results = {}
    for file in py_files_changed:
        pylint = Popen(("pylint --disable=R,C %s" % file).split(),
                       stdout=PIPE)
        pylint.wait()

        output = pylint.stdout.read().decode('ascii').strip()
        print (output)

        results_re = re.compile(r"Your code has been rated at ([\d\.]+)/10")
        results[file] = float(results_re.findall(output)[0])

    # Display a summary of the results (if any files were checked).
    if len(results.values()) > 0:
        print ("===============================================")
        print ("Final Results:")
        for file in results:
            result = results[file]
            grade = "FAIL" if result < PYLINT_PASS_THRESHOLD else "pass"
            print ("[ %s ] %s: %.2f/10" % (grade, file, result))


    # If any of the files failed the Pylint test, exit nonzero and stop the
    # commit from continuing.
    # currently pass_threshold is set to 0. commit won't fail. 
    if any([(result < PYLINT_PASS_THRESHOLD)
            for result in results.values()]):
        # print ("git: fatal: commit failed, Pylint tests failing.")
        sys.exit(1)
    else:
        sys.exit(0)

def is_py_script(filename):
    """Returns True if a file is a python executable."""

    # os.X_OK Checks if path can be executed.
    if not os.access(filename, os.X_OK):
        return False
    else:
        try:
            # check if a file will execute in python if the first line is #!/usr/local/bin/python
            first_line = open(filename, 'r').readline()
            return ("#!" in first_line) and ("python" in first_line)
        except StopIteration:
            return False


if __name__ == "__main__":
    main()