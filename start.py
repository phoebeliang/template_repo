#!/usr/local/bin/python
"""
A python script to install the precommit hook automatically 
on the client side when a new repo is created. 

this is written in python so it can be run in both Windows and Linux Command

"""
import os, pandas

precommit_file = "./pre-commit.py"
new_file = "./.git/hooks/pre-commit"

with open(precommit_file) as f1: 
    lines = f1.readlines()
    lines = [l for l in lines]

    with open(new_file, "w") as f2: 
        f2.writelines(lines)

#print ("hello")